#pragma once
#include "ray.h"

class camera
{
public:
    vec3 origin;
    vec3 lower_left_corner;
    vec3 horizontal;
    vec3 vertical;
    camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect) {
        // vfov is top to bottom in degrees
        vec3 u, v, w;
        float theta = glm::radians(vfov);
        float half_height = glm::tan(theta/2);
        float half_width = aspect * half_height;
        origin = lookfrom;
        w = glm::normalize(lookfrom - lookat);
        u = glm::normalize(cross(vup, w));
        v = glm::cross(w, u);
        lower_left_corner = origin - half_width*u - half_height*v - w;
        horizontal = 2*half_width*u;
        vertical = 2*half_height*v;
    }

    
    ray get_ray(const vec2& uv) const
    { return ray(origin, lower_left_corner + uv.x*horizontal + uv.y*vertical - origin); }
};