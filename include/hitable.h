#pragma once
#include "ray.h"

class material;//forward declaration

struct hit_record{
    float t;
    vec3 p;
    vec3 normal;
    material* mat_ptr;
};

class hitable {
public:
    virtual bool hit (const ray& r, float t_min, float t_max, hit_record& rec) const = 0;
};

class sphere: public hitable {
public:
    vec3 center;
    float radius;
    material* mat_ptr;
    sphere(){}
    sphere(vec3 cen, float r, material* mptr) :center(cen), radius(r), mat_ptr(mptr){}
    virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;
};

class hitable_list: public hitable{
public:
    hitable **list;
    int size;
    hitable_list(hitable** l, int size): list(l), size(size){}
    virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;
};