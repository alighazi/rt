#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
using glm::vec3;
using glm::vec2;

class ray{
public:
    static float frand(){
        return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    }
    vec3 A;
    vec3 B;
    ray(){}
    ray(const vec3& a, const vec3& b) {A = a; B= b;}
    vec3 origin() const {return A;}
    vec3 direction() const {return B;}
    vec3 point_at_parameter(float t) const  {return A + t*B;}
};