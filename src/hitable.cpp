#include "hitable.h"

bool sphere::hit(const ray& r, float tmin, float tmax, hit_record& rec) const{
    /* Sphere formula:
    A: ray Origin, B: ray Direction, C: sphere Center, R: sphere Radius
    t*t*dot(B,B) + 2*t*dot(B,A-C) + dot(A-C, A-C) - R*R = 0
    this is a second degree equation of the form ax^2 + bx + c = 0 where x = ta so:
    a = dot (B,B) , b= 2*dot(B, A-C), c= dot(A-C, A-C) - R*R
    */
    vec3 o_c = r.origin() - center;//A - C
    float a = glm::dot(r.direction(), r.direction());
    float b = glm::dot(o_c, r.direction());// we omit 2.0* cancelled out by the simplifications at the next stages
    float c = glm::dot(o_c, o_c) - radius * radius;
    float discriminant = b*b - a*c;//4* is also omitted
    if(discriminant >0.f){
        float root  = (-b - glm::sqrt(discriminant)) /a;// note that it is (a) not 2*a because we canceled the 2* and 4* above
        if(root < tmax && root > tmin){// test if the smaller root is in range
            rec.t = root;
            rec.p = r.point_at_parameter(root);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = this->mat_ptr;
            return true;
        }
        root = (-b + glm::sqrt(discriminant)) /a;
        if(root < tmax && root > tmin){// test if the bigger root is in range
            rec.t = root;
            rec.p = r.point_at_parameter(root);
            rec.normal = (rec.p - center) / radius;
            rec.mat_ptr = this->mat_ptr;
            return true;
        }
    }

    return false;
}

bool hitable_list::hit(const ray& r, float tmin, float tmax, hit_record& rec) const{
    hit_record temp_rec;
    bool hit_anything = false;
    float closest = tmax;
    for(int i=0; i< size; i++){
        if(list[i]->hit(r, tmin, closest, temp_rec)){
            hit_anything = true;
            closest = temp_rec.t;
            rec = temp_rec;
        }
    }
    return hit_anything;
}
