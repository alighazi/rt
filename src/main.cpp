#include <cstdio>
#include <cstdlib>
#include <cfloat>
#include <ctime>

#include "hitable.h"
#include "camera.h"
#include "material.h"

vec3 color(const ray& r, const hitable_list& world, int depth){
    hit_record rec;
    if(world.hit(r, 0.001f, FLT_MAX, rec)){
        ray scattered;
        vec3 attenuation;
        if(depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)){
            return attenuation * color(scattered, world, depth +1);
        }else{
            return vec3(0.f);
        }
    }
    
    vec3 unit_dir = glm::normalize(r.direction());
    float t = 0.5f * (unit_dir.y + 1.f);
    return vec3(1.f-t, 1.f-t, 1.f);
}
void ray_trace(){
    int nx = 800;
    int ny = 600;
    int ns = 100;//samples
    
    std::FILE *f;
    errno_t e = fopen_s(&f, "output/9.ppm", "w");
    if (e || !f) {
        fprintf(stderr, "failed to open, error no.: %d\n", e);\
        return;
    }

    fprintf(f, "P3\n%d %d\n255\n", nx, ny);
    const int size = 4;
    hitable* arr[size];
    float R = cos(glm::quarter_pi<float>());

    arr[0] = new sphere(vec3(0.f, 0.f, -1.f), 0.5f, new lambertian(vec3(0.8f, 0.3f,0.3f)));
    arr[1] = new sphere(vec3(0.f, -100.5f, 1.f), 100.f, new lambertian(vec3(0.8f, 0.8f, 0.f)));
    arr[2] = new sphere(vec3(1.f, 0.0f, -1.f), 0.5f, new metal(vec3(0.8f, 0.6f, 0.2f)));
    arr[3] = new sphere(vec3(-1.f, 0.0f, -1.f), 0.5f, new dielectric(1.5f));

    //arr[0] = new sphere(vec3(-R,0,-1), R, new lambertian(vec3(0, 0, 1)));
    //arr[1] = new sphere(vec3( R,0,-1), R, new lambertian(vec3(1, 0, 0)));
    hitable_list world(arr, size);
    camera cam(vec3(-2.f, 2.f, 1.f), vec3(0.f, 0.f, -1.f), vec3(0.f, 1.f, 0.f), 60.f, static_cast<float>(nx)/ny);
    auto t1=time(nullptr);
    for(int j= ny-1; j >= 0; j--){
        printf("%.2f complete\n", 100.f - (j*100.f/ny));
        for(int i = 0; i < nx; i++){
            vec3 col(0.f);
            for(int s =0; s < ns; s++){
                vec2 rand = vec2(ray::frand(), ray::frand());
                vec2 uv = (vec2(i, j) + rand) / vec2(nx, ny);
                ray r = cam.get_ray(uv);
                col += color(r, world, 0);
            }
            col /= static_cast<float>(ns);
            col = glm::sqrt(col);
            int ir = static_cast<int>(255.99f*col.x);
            int ig = static_cast<int>(255.99f*col.y);
            int ib = static_cast<int>(255.99f*col.z);
            fprintf(f, "%d %d %d\n", ir, ig, ib);
        }
    }
    auto t2=time(nullptr);
    printf("finished in %lld seconds\n", t2-t1);
}

int main(int argc, char **argv) {    
    //sdlgl();
    ray_trace();
    return 0;
} 